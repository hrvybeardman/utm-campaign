<?php

include 'vendor/autoload.php';

//Register an autoloader
$loader = new \Phalcon\Loader();
$loader->registerDirs(
    array(
        '../app/controllers/',
        '../app/models/'
    )
)->register();

//Create a DI
$di = new Phalcon\DI\FactoryDefault();

//Register Volt as template engine with an anonymous function
$di->set('view', function() {

    $view = new \Phalcon\Mvc\View();

    $view->setViewsDir('../app/views/');

    $view->registerEngines(array(
        ".volt" => function($view, $di) {
                $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

                $volt->setOptions(array(
                    "compiledPath" => "../app/storage/views/",
                    "compiledExtension" => ".php",
                    "compiledSeparator" => '%%',
                    "compileAlways" => true
                ));

                return $volt;
            }
    ));

    return $view;
});

$di->set('shortener', function() use ($di) {
    return new Adobradi\Services\Shortener($di);
});

new Whoops\Provider\Phalcon\WhoopsServiceProvider;

//Handle the request
$application = new \Phalcon\Mvc\Application();
$application->setDI($di);
echo $application->handle()->getContent();
