(function($) {
    var Shortener = function() {
        this.init();
    };

    Shortener.prototype.init = function() {
        var self = this;
        $(document).on('submit','#campaign-form',function(e) {
            e.preventDefault();
            self.longUrl = self.createUrl($(this));
            if ( $(this).find('#shorten').first().prop('checked') ) {
                self.shorten(function(data) {
                    self.show(data.longUrl, data.id);
                });
            } else {
                self.show(self.longUrl);
            }
        });
    };

    Shortener.prototype.createUrl = function($el) {
        var elements = {};
        $el.find('input').each(function() {
            var name = $(this).attr('id');
            var val = $(this).val();
            elements[name] = val;
        });

        var longUrl = elements.url;
        var firstChar = ( longUrl.indexOf('?') == -1 ? '?' : '&' );
        longUrl += firstChar + 'utm_source=' + elements.source;
        longUrl += '&utm_medium=' + elements.medium;
        if ( elements.term.length > 0 ) { longUrl += '&utm_term=' + elements.term; }
        if ( elements.content.length > 0 ) { longUrl += '&utm_content=' + elements.content; }
        longUrl += '&utm_campaign=' + elements.name;

        return longUrl;
    }

    Shortener.prototype.shorten = function(callback) {
        var longUrl = this.longUrl;
        $.getJSON('/shortener',{url: longUrl},callback);
    }

    Shortener.prototype.show = function(longUrl, shortUrl) {
        $('#longUrl').val(longUrl);
        if ( null != shortUrl ) {
            $('#shortUrl').val(shortUrl);
        } else {
            $('#shortUrl').parents('.wrap').first().hide();
        }

        $('#result').modal();
    }

    var shortener = new Shortener();
})($);