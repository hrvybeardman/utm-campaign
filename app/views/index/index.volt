{% extends "layout/master.volt" %}

{% block title %}
    Home
{% endblock %}

{% block content %}
<div class="row">
<form role="form" id="campaign-form" class="col-md-6 col-md-offset-3 col-sm-12">
    <div class="form-group">
        <label for="url">Base URL</label>
        <input required="required" type="url" class="form-control" id="url" placeholder="Eg. http://www.google.com">
        <span class="help-block"><b>Required</b> The URL you want to track</span>
    </div>
    <div class="form-group">
        <label for="source">Campaign source</label>
        <input required="required" type="text" class="form-control" id="source" placeholder="Eg. google, twitter, facebook">
        <span class="help-block"><b>Required</b> Use it to identify the source of a hit</span>
    </div>
    <div class="form-group">
        <label for="medium">Campaign medium</label>
        <input required="required" type="text" class="form-control" id="medium" placeholder="Eg. cpc, banner">
        <span class="help-block"><b>Required</b> Use it to identify the medium the hit was received from</span>
    </div>
    <div class="form-group">
        <label for="term">Campaign term</label>
        <input type="text" class="form-control" id="term">
        <span class="help-block">Used for paid search</span>
    </div>
    <div class="form-group">
        <label for="content">Campaign content</label>
        <input type="text" class="form-control" id="content">
        <span class="help-block">Used in cases links pointing to the same URL</span>
    </div>
    <div class="form-group">
        <label for="name">Campaign name</label>
        <input required="required" type="text" class="form-control" id="name">
        <span class="help-block"><b>Required</b> Name of the campaign or promotion</span>
    </div>
    <div class="checkbox">
        <label>
            <input id="shorten" type="checkbox"> Shorten link
        </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
{% endblock %}