<div id="result" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="longUrl">Long URL: <input type="url" name="longUrl" id="longUrl" /></label>
                    </div>
                    <div class="form-group">
                        <label for="shortUrl">Short URL: <input type="url" name="shortUrl" id="shortUrl" /></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->