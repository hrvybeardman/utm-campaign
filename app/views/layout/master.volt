<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{% block title %}Home{% endblock %} - URL Generator for UTM Campaigns</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    {% include "include/header.volt" %}
    <div class="container">
        {% block content %}{% endblock %}
    </div>

    {% include "include/modals.volt" %}

    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/app/js/app.js"></script>
</body>
</html>
