<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class ShortenerController extends Controller {

    public function indexAction()
    {
        $url = $_GET['url'];

        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        $response->setContent(json_encode($this->shortener->shorten($url)));

        return $response;
        $this->view->disable();
    }
} 